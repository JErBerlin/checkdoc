## An app to check the sharpness of documents

### Overview

This app measures the sharpness and other readability conditions of the document

## How to run...

If you have Go installed on your computer, you can just get this repository and run this app locally (you can install the go compiler here: https://golang.org/doc/install).

If you would not like to install a Go compiler, please read further in the section about how to run the application in a docker container.

###  the compiler

`go build`

###  the program

To run the app, you can just type:

`go run checkdoc`

(Observe that it will be listening at port 8080. If you get an error when starting the server because the port is already in use, try changing the value of the port constant in the file `main.go`)

### How to post an image to the app to get sharpness measure

When testing locally, the API will be listening and serving http at port 8080 of the localhost.

- to send your image, make a POST request at http://localhost:8080/analyse/sharpness

For instance, using the curl command and the file auClear.jpg from the resources' folder, your request could look like this:

```
curl -XPOST "http://localhost:8080/analyse/sharpness" \

-F "file=@auClear.jpg" \

-H "Content-Type: multipart/form-data"
```

### How to run the app from the Docker image

There is a provided Dockerfile to run the application in a docker container.

To build a new image you can type this from the directory where the Dockerfile is:

`docker build -t checkdoc .`

To run the docker container (you can also omit the `-it` option):

`docker run -it -p 8080:8080 --name=checkdoc checkdoc`

You can stop the container pressing ctrl+C or typing following from a new terminal tab:

`docker stop checkdoc`
