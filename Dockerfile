FROM golang:1.16 as go-builder

WORKDIR /app

COPY . .
ARG GOPROXY=https://goproxy.io

RUN go mod download \
    && pwd && ls \
    && go install github.com/gobuffalo/packr/packr && \
    CGO_ENABLED=0 packr build -o checkdoc


FROM alpine:latest as prod
COPY --from=go-builder /app/checkdoc /app/checkdoc

WORKDIR /app

ENTRYPOINT ["/app/checkdoc"]