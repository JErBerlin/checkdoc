module checkdoc

go 1.16

require (
	github.com/disintegration/imaging v1.6.2
	github.com/gobuffalo/packr v1.30.1 // indirect
	github.com/mjibson/go-dsp v0.0.0-20180508042940-11479a337f12
)
