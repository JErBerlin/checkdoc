package main

import (
	"fmt"
	"github.com/disintegration/imaging"
	"github.com/mjibson/go-dsp/fft"
	"image"
	"image/jpeg"
	"io"
	"log"
	"math"
	"math/cmplx"
	"mime/multipart"
	"net/http"
	"os"
)

const port = ":8080"

func main() {
	analyseSharpnessHdl := http.HandlerFunc(sharpness)
	pingHdl := http.HandlerFunc(ping)

	http.Handle("/analyse/sharpness", analyseSharpnessHdl)
	http.Handle("/", pingHdl)

	log.Println("listen on", port)
	log.Fatal(http.ListenAndServe(port, nil))
}

// makeImageFile reads the binary information from the request body and returns a multpart file with its header
func makeImageFile(w http.ResponseWriter, request *http.Request) (multipart.File, *multipart.FileHeader) {
	err := request.ParseMultipartForm(32 << 20) // maxMemory 32MB
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("bad request")
		return nil, nil
	}

	file, h, err := request.FormFile("file")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("bad request")
		return nil, nil
	}

	return file, h
}

// ping does a simple health check
func ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	log.Println("ping -> pong")

	io.WriteString(w, `{"alive": true}`+"\n")
}

// sharpness gives back a measure of readability of the image in the request and writes the value as JSON in the writer
// In case of error, it writes the error and returns
func sharpness(w http.ResponseWriter, r *http.Request) {
	file, h := makeImageFile(w, r)
	var inImage image.Image
	var inImageName string
	var err error

	if file == nil || h == nil {
		inImageName = "auClear.jpg"
		inImage, err = getImageFromFilePath("./resources/" + inImageName)
	} else {
		inImageName = h.Filename
		inImage, err = getImageFromFile(file, h)
	}

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		response := fmt.Sprintf(`{"error": "%s"}`, err)
		io.WriteString(w, response)
		log.Printf("error getting image: %s\n", err)
		return
	}

	sharpness := sharpValue(inImage)
	response := fmt.Sprintf(`{"filename": "%s",`+`"sharpness": %2.2f}`, inImageName, sharpness)

	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, response)
	log.Printf("ok: %s\n", response)
}

// applyFFTTransformation makes a fast fourier transform of the matrix
func applyFFTTransformation(m [][]float64) [][]complex128 {
	return fft.FFT2Real(m)
}

// invFFTTransformation gives the values back
// in case we want to reconstruct the original image
func invFFTTransformation(m [][]complex128) [][]complex128 {
	return fft.IFFT2(m)
}

// getImageFromFilePath reads from file path to image
// valid formats are among others jpg and jpeg
func getImageFromFilePath(filePath string) (image.Image, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("could not read the file %s: %s", filePath, err)
	}
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil, fmt.Errorf("colud not decode the file %s: %s", filePath, err)
	}

	return img, nil
}

// getImageFromFile reads from file to image
// valid formats are among others jpg and jpeg
func getImageFromFile(f multipart.File, h *multipart.FileHeader) (image.Image, error) {
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil, fmt.Errorf("colud not decode the file %s: %s", h.Filename, err)
	}

	return img, nil
}

// saveImageToFilePath saves an image in a given path in format jpeg
func saveImageToFilePath(img image.Image, filePath string) error {
	// Save dstImage in a file
	f, err := os.Create(filePath)
	if err != nil {
		// Handle error
	}
	defer f.Close()

	// Specify the quality, between 0-100
	opt := jpeg.Options{
		Quality: 90,
	}
	err = jpeg.Encode(f, img, &opt)
	if err != nil {
		// Handle error
	}
	return err
}

// blurImage blurs image according to factor
func blurImage(img image.Image, factor float64) *image.NRGBA {
	return imaging.Blur(img, factor)
}

// toGray Converts image to grayscale
func toGray(img image.Image) *image.Gray {
	grayImg := image.NewGray(img.Bounds())

	for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
		for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
			grayImg.Set(x, y, img.At(x, y))
		}
	}

	return grayImg
}

// toPixelMatrix converts image to pixel matrix
func toPixelMatrix(img image.Image) [][]float64 {

	bounds := img.Bounds()
	realPixels := make([][]float64, bounds.Dy())

	for y := 0; y < bounds.Dy(); y++ {
		realPixels[y] = make([]float64, bounds.Dx())
		for x := 0; x < bounds.Dx(); x++ {
			r, _, _, _ := img.At(x, y).RGBA()
			realPixels[y][x] = float64(r)
		}
	}

	return realPixels
}

// meanComplexMatrix returns the mean of the abs values of a complex matrix
func meanComplexMatrix(a [][]complex128, m int, n int) float64 {
	var r float64

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			r += cmplx.Abs(a[i][j])
		}
	}

	return r / (float64(m) + float64(n) + 2)
}

// sharpValue returns a measure of the sharpness of an image
// adjusted for AU-Scheine (German sick leave sheets)
func sharpValue(img image.Image) float64 {
	grayImage := toGray(img)

	// convert to pixel matrix
	bounds := grayImage.Bounds()

	realPixels := make([][]float64, bounds.Dy())

	n := bounds.Dy()
	m := bounds.Dx()

	realPixels = toPixelMatrix(grayImage)

	// apply discrete fourier transform on realPixels
	coeffs := applyFFTTransformation(realPixels)

	return math.Log10(meanComplexMatrix(coeffs, n, m))*60 - 476
}
